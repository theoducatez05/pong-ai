﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    protected Camera cameraToLookAt;

    void Start()
    {
        this.cameraToLookAt = FindObjectOfType<Camera>();
    }

    void Update()
    {
        rotateTowardCamera();
    }

    protected void rotateTowardCamera()
    {
        if (this.cameraToLookAt != null)
        {
            transform.rotation = Quaternion.LookRotation(this.transform.position - cameraToLookAt.transform.position);
        }
    }
}
