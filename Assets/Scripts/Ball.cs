﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public PlayerAgent Player1;
    public PlayerAgent Player2;

    public float ballOldVelocity = 0f;
    public float ballVelocity = 0f;

    public Rigidbody paramRigidbody;

    float speed = 0;
    public void setVelocity(float pv)
    {
        speed = pv;
    }
    // Start is called before the first frame update
    void Start()
    {
        paramRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v = GetComponent<Rigidbody>().velocity;

        if (v.x < 0 && v.x > -10f)
        {
            v.x = -10f;
        }
        else if (v.x > 0 && v.x < 10f)
        {
            v.x = 10f;
        }

        if (v.z <= 0 && v.z > -1)
        {
            v.z = Random.Range(0f, -4f);
        }
        else if (v.z > 0f && v.z < 1f)
        {
            v.z = Random.Range(0f, 4f);
        }
        v = v.normalized;
        //TRAINED WITH 10
        v *= speed;
        GetComponent<Rigidbody>().velocity = v;

        ballVelocity = paramRigidbody.velocity.x;
        //player2
        if (transform.localPosition.x > 1)
        {
            if (ballVelocity < 0 && ballOldVelocity > 0)
            {
                this.Player1.AddReward(0.1f);
            }
        }
        else
        {
            if (ballVelocity > 0 && ballOldVelocity < 0)
            {
                this.Player2.AddReward(0.1f);
            }
        }

        ballOldVelocity = ballVelocity;
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(collision.gameObject == Player1)
    //    {
    //        Player1.GetComponent<PlayerAgent>().SetReward(0.5f);
    //        //Player2.GetComponent<PlayerAgent>().SetReward(-0.02f);
    //    }
    //    else if (collision.gameObject == Player2)
    //    {
    //        Player2.GetComponent<PlayerAgent>().SetReward(0.5f);
    //        //Player1.GetComponent<PlayerAgent>().SetReward(-0.02f);
    //    }
    //}

    private void LateUpdate()
    {
        //GetComponent<Rigidbody>().velocity = v;
    }
}
