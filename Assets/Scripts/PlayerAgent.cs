﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class PlayerAgent : Agent
{
    public GameObject OtherPlayer;
    public Ball Ball;
    public TextMesh meshScore;
    private int score;
    Rigidbody BallRB;

    float ForceMultiplier = 320f;
    // Start is called before the first frame update
    void Start()
    {
        BallRB = Ball.GetComponent<Rigidbody>();
        this.score = 0;
    }

    public override void OnEpisodeBegin()
    {
        this.Ball.ballVelocity = 0;
        this.Ball.ballOldVelocity = 0;

        if (GetComponent<BehaviorParameters>().TeamId == 0)
        {
            if (Random.value > 0.5f)
            {
                Ball.transform.localPosition = new Vector3(7f, 0.375f, 0f);
                BallRB.AddForce(GetBallInitForce(true), ForceMode.Force);
            }
            else
            {
                Ball.transform.localPosition = new Vector3(-7f, 0.375f, 0f);
                BallRB.AddForce(GetBallInitForce(false), ForceMode.Force);
            }
            //Ball.transform.localPosition = new Vector3(-9.35f, 0.375f, 0f);
            //BallRB.AddForce(GetBallInitForce(false), ForceMode.Force);
            BallRB.velocity = new Vector3();
            Ball.setVelocity(20f);//Random.Range(20, 25f));
        }
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Ball.transform.localPosition.x);
        sensor.AddObservation(Ball.transform.localPosition.z);

        //Player position
        //sensor.AddObservation(this.transform.localPosition.x);
        sensor.AddObservation(this.transform.localPosition.z);

        // Opponent position
        //sensor.AddObservation(OtherPlayer.transform.localPosition.x);
        //sensor.AddObservation(OtherPlayer.transform.localPosition.z);

        //Ball velocity
        sensor.AddObservation(BallRB.velocity.x);
        sensor.AddObservation(BallRB.velocity.z);
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        Vector3 controlSignal = Vector3.zero;
        // Get the action index for movement
        int movement = Mathf.FloorToInt(vectorAction[0]);

        //Check if need to Randomize movement
        if(gameObject.GetComponent<BehaviorParameters>().BehaviorType == BehaviorType.InferenceOnly && GameManager.Instance.CurrentDifficulty == GameManager.DifficultyManagerMode.Random && RandomizeAction())
        {
            Debug.Log("Randomize");
            movement = 0;
        }

        if (movement == 0)
            controlSignal.z = 0;
        else if (movement == 1)
            controlSignal.z = 0.2f;
        else if (movement == 2)
            controlSignal.z = -0.2f;
        if (controlSignal.z > 0.2f)
            controlSignal.z = 0.2f;
        if (controlSignal.z < -0.2f)
            controlSignal.z = -0.2f;
        transform.Translate(controlSignal);
        if (transform.localPosition.z > 3.85f)
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 3.85f);
        if (transform.localPosition.z < -3.85f)
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -3.85f);
    }

    public override void Heuristic(float[] actionsOut)
    {
        //float speed = 1f;
        if (Input.GetKey(KeyCode.RightArrow))
            actionsOut[0] = 2;
        else if (Input.GetKey(KeyCode.LeftArrow))
            actionsOut[0] = 1;
        else
            actionsOut[0] = 0f;
    }

    Vector3 GetBallInitForce(bool pos)
    {
        //float x = Random.Range(-1 * ForceMultiplier, 1 * ForceMultiplier);
        float x = -1f * ForceMultiplier;
        if (pos)
            x = 1f * ForceMultiplier;
        float z = Random.Range(-150f, 150);
        //Debug.Log(x);
        return -new Vector3(x, 0, z).normalized * 500f;
    }
    
    private bool RandomizeAction()
    {
        if(GameManager.Instance.CurrentScorePlayer2 > GameManager.Instance.CurrentScorePlayer1 - 4)
        {
            int indicateur = Mathf.Max(1, GameManager.Instance.CurrentScorePlayer2 - GameManager.Instance.CurrentScorePlayer1);
            if (Mathf.Min(Random.value * indicateur, 0.9f * indicateur) > 2.7f)
            {
                return true;
            } else
                return false;
        }else
        {
            return false;
        }
    }

    public void AddScore()
    {
        this.score++;
        this.meshScore.text = this.score.ToString();
    }
}
