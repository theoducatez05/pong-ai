﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{

    public PlayerAgent WinnerLinked;
    public PlayerAgent LooserLinked;
    public bool PlayerDown;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            LooserLinked.AddReward(-1f);
            WinnerLinked.AddReward(1f);
            LooserLinked.EndEpisode();
            WinnerLinked.EndEpisode();
            //Update score if initial
            if (transform.parent.parent.transform.position.x == 0 && transform.parent.parent.transform.position.y == 0)
            {
                GameManager.Instance.AddScore(!PlayerDown);
            }

            //update new score
            WinnerLinked.AddScore();
        }
    }
}
