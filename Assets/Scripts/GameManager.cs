﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public enum DifficultyManagerMode {None, Random }
    public DifficultyManagerMode CurrentDifficulty;
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public Text ScorePlayer1;
    public Text ScorePlayer2;
    [HideInInspector]
    public int CurrentScorePlayer1 = 0;
    [HideInInspector]
    public int CurrentScorePlayer2 = 0;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddScore(bool IsPlayerUp)
    {
        Debug.Log("Player down scored : " + IsPlayerUp);
        if (IsPlayerUp)
            CurrentScorePlayer1++;
        else
            CurrentScorePlayer2++;
        ScorePlayer1.text = CurrentScorePlayer1.ToString();
        ScorePlayer2.text = CurrentScorePlayer2.ToString();
    }
}
