# Pong reinforcement learning project

This page is meant to summarize the project. If you need more details on the process, read my full documentation paper (in french) here : [Theo_Ducatez_pong_AI_project.pdf](./doc/Theo_Ducatez_pong_AI_project.pdf)

## Table of Contents

* [About the project](#about-the-project)
* [Methodology](#methodology)
* [Results](#results)
* [Built With](#built-with)
* [Creators](#creators)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



## About the project

<div align="center">
  <img src="Gitlab_images/pong_big.JPG" width="650" height="350"/>
</div>
<br/>

This project was made during the second semester of 2020 and is a mix between game prototyping and machine learning. I wanted to work on a project mixing thoses two fields as I am passionate about game programming and have a rising interest in machine learning. After some researches on the tools I could use I found the perfect plugin to use for this project "Ml-Agent". A plugin made by Unity making it possible to train intelligent agents in a Unity virtual environment. It is fairly new, free and easy to use. I documented the steps needed in order to install it and too use it properly in the documentation available at the top of this document.

The idea to implement it into a pong video game comes from the simplicity needed to create this type of game and its originality. Pong games AI are always hard-coded and I found it interresting to try to implement a self-learning agent into this type of game. The game was easy and fast to create, allowing me to focus more on the reinforcement learning side of the project.   

The results of the different trainings are available in the [results](#results) part of this document and are just a summary. For more information, you can read the documentation. In the end, I am very proud of the resulting AI and I am very happy to have learned more about artificial intelligence and reinforcment learning as well as learning to use a new plugin for Unity.

## Methodology

Here is how I proceeded to undertake the realisation of this project:

* **Documentation phase**
  * In this phase, I documented myself on the way the plugin worked, the technologies behind it, the concepts and how to implement it properly. At the same time I wrote a documentation document in order to summarize all of this information.<br/><br/>
* **Game progamming phase**
    * During this phase I created the pong game and all the mechanics.<br/>

<div align="center">
  <img src="Gitlab_images/pong_1.PNG" width="100" height="250"/>
</div>

* **AI behaviour analysis (rewards, actions, observations)**
    * During this phase I had reflexions on the way the AI would work and how to implement it. I followed my documentation and wrote down my analysis for the way the agent would act, how awards would be given and how the agent would percieve its environement.<br/>

<div align="center">
  <img src="Gitlab_images/yml_config.PNG" width="150" height="250"/>
</div>

* **AI implementation**
    * Then I implemented this logic into the game with the pluging and tested it.<br/><br/>
* **AI training**
    * When everything worked, I started the training multiple times and tweeked several times the implemented logic to train efficiently the agents. When I was happy with the results I saved the resulting models. The process took around three to four months of work (during my free time after school) and each training sessions would last 10 to 15 hours in average

<div align="center">
  <img src="Gitlab_images/pong_2.PNG" width="200" height="300"/>
</div>
<br/>


## Results

**1 -** This is the first result I had when training my agents.

<div align="center">
  <img src="Gitlab_images/pong_3_rew.PNG" width="500" height="325"/>
</div>

We can see that the IA got better with time but in the end it wasn't efficient at all. I didn't succeed in not taking any goals and wasn't really precise. I had to find another solution. This is how I planed the reward system: 

* Rewards 
  * +1.0 when goal in the other side
  * -1.0 when take a goal
  * +0.1 when hit the ball<br/><br/>

* Observations
  * The ball position
  * The opponent position<br/><br/>

* Actions
  * Continuous with a float

---
<br/>

**2 -** Later on I decided to train my agents differently. I decided to get rid of the opponent information as the agent can't really control where it sends the ball, tehrefore it cannot aim where the oppenent isn't in order to goal. Then I decided to add the ball velocity to the observations for the agent to predict where the ball would be when it will touch it.

<div float="left" align="center">
  <img src="Gitlab_images/pong_4_rew.PNG" width="500" height="325"/>
  <img src="Gitlab_images/pong_4_elo.PNG" width="500" height="325"/>
</div>

We can see that the agent is way more efficient. The cumulative reward is rising way faster because the agents took less and less goals and therefore less punishments. The self-play data it way better too because they play way more efficiently. This is how I planed the reward system: 

* Rewards 
  * -1.0 when take a goal
  * +0.1 when sends back the ball in the other direction (not only touch)<br/><br/>

* Observations
  * The ball position
  * The ball velocity<br/><br/>

* Actions
  * Continuous with a float

---
<br/>

**3 -** The precedent AI was very strong but I wondered if I could get an even stronger AI by tweeking some parameters. I remembered that an other way of implementing actions made by agents existed. The "discrete" mode, which is said to be more efficient for actions that doesn't require any floats to move. It was perfect for the pong game because the agents had only three actions they could do, move left, move right and not moving. The way I did it before was with a float that would go from -1 to 1 in order to dictate in which side the agent would move. With the new method, the agent would be more precise and more efficient.

<div float="left" align="center">
  <img src="Gitlab_images/pong_5_rew.PNG" width="500" height="325"/>
  <img src="Gitlab_images/pong_5_elo.PNG" width="500" height="325"/>
</div>

We can see that the agent is way way more efficient. The agents are training faster and in the end, the agent is almost unbeatable. I only succed in getting 4 points out of 50 when I play against it when I am lucky. I am very proud of the result. This is how I planed the reward system: 

* Rewards 
  * -1.0 when take a goal
  * +0.1 when sends back the ball in the other direction (not only touch)<br/><br/>

* Observations
  * The ball position
  * The ball velocity<br/><br/>

* Actions
  * Discrete action tables


## Built With

* [Unity](https://unity.com)
* [Ml-Agent](https://unity.com/products/machine-learning-agents)
* [Tensorflow](https://www.tensorflow.org)
* [Tensorboard](https://www.tensorflow.org/tensorboard)


## Creators

* Théo Ducatez (Computer science engineering student)
* Yann Kerkhof (Computer science engineering student)

## Contact

Théo Ducatez - [theo-ducatez](https://www.linkedin.com/in/theo-ducatez/) - theoducatez05@gmail.com

Project Link: [https://gitlab.com/theoducatez05/pong-ai](https://gitlab.com/theoducatez05/pong-ai)



## Acknowledgements
* [Unity](https://unity.com/) For its wonderful plugin
* Ms Domitille Lourdeaux, VR researcher at UTC engineering school for her help

